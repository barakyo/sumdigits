import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by barackkaravani on 9/7/16.
 */
public class SumDigitsSpec {
    SumDigits sumDigits;
    @Before
    public void setup() {
        sumDigits = new SumDigits();
    }

    @Test
    public void testGetSum() {
        assertEquals(1, sumDigits.getSum(1));
        assertEquals(4, sumDigits.getSum(13));
        assertEquals(7, sumDigits.getSum(133));
        assertEquals(14, sumDigits.getSum(1337));
    }
}
